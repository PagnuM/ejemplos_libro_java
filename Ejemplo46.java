
// Fig. 7.22: ManipulacionesArreglos.java
// Métodos de la clase Arrays y System.arraycopy.
import java.util.Arrays;

public class Ejemplo46 {
    public static void main(String[] args) {

        // ORDENAMIENTO
        double[] arregloDouble = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        Arrays.sort(arregloDouble);

        System.out.println("\narregloDouble: ");
        for (double d : arregloDouble)
            System.out.printf("%.1f ", d);
        System.out.println();

        // LLENAR
        int[] arregloIntLleno = new int[10];
        Arrays.fill(arregloIntLleno, 7);
        mostrarArreglo(arregloIntLleno, "arregloIntLleno");
        System.out.println();

        // COPIA
        int[] arregloInt = { 1, 2, 3, 4, 5, 6 };
        int[] copiaArregloInt = new int[arregloInt.length];
        System.arraycopy(arregloInt, 0, copiaArregloInt, 0, arregloInt.length);
        mostrarArreglo(arregloInt, "arregloInt");
        mostrarArreglo(copiaArregloInt, "copiaArregloInt");
        System.out.println();

        // COMPARAR ARREGLOS
        boolean b = Arrays.equals(arregloInt, copiaArregloInt);
        System.out.printf("%narregloInt %s copiaArregloInt%n", (b ? "==" : "!="));

        b = Arrays.equals(arregloInt, arregloIntLleno);
        System.out.printf("%narregloInt %s arregloIntLleno%n", (b ? "==" : "!="));

        // BUSQUEDA BINARIA
        int ubicacion = Arrays.binarySearch(arregloInt, 571);
        if (ubicacion >= 0)
            System.out.printf("Se encontro el elemento en el elemento %d del arregloInt%n", ubicacion);
        else
            System.out.println("No se encontro el elemento en arregloInt");
    }

    public static void mostrarArreglo(int[] arreglo, String descripcion) {
        System.out.printf("%n%s: ", descripcion);
        for (int i : arreglo)
            System.out.printf("%d ", i);
    }
}