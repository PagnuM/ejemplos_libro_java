// Fig. 3.2: PruebaCuenta.java
// Crear y manipular un objeto Cuenta.
package Ejemplo08;

import java.util.Scanner;

public class PruebaCuenta {
    public static void main(String[] args) {
        Cuenta cuenta1 = new Cuenta();
        System.out.printf("El valor inicial de nombre es: %s\n", cuenta1.obtenerNombre());

        Scanner scan = new Scanner(System.in);
        System.out.print("Ingrese un nombre para la cuenta: ");
        String nombre = scan.next();
        cuenta1.establecerNombre(nombre);

        String nombreCuenta = cuenta1.obtenerNombre();
        System.out.printf("El valor setteado del nombre es: %s\n", nombreCuenta);
    }
}