// Fig. 3.1: Cuenta.java
// Clase Cuenta que contiene una variable de instancia llamada nombre
// y métodos para establecer y obtener su valor (Getters y Setters).
package Ejemplo08;

public class Cuenta {
    // Variable de instancia
    private String nombre;

    // Setter
    public void establecerNombre(String nombre) {
        this.nombre = nombre;
    }

    // Getter
    public String obtenerNombre() {
        return this.nombre;
    }

}