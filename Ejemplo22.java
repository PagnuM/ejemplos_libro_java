import javax.swing.JFrame;

// Fig. 4.19: PruebaPanelDibujo.java
// Crear un objeto JFrame para mostrar un objeto PanelDibujo.
public class Ejemplo22 {
    public static void main(String[] args) {
        Ejemplo21 panel = new Ejemplo21();
        JFrame frame = new JFrame();

        frame.add(panel);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 250);
        frame.setVisible(true);
    }
}