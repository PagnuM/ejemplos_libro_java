// Fig. 7.17: InicArreglo.java
// Inicialización de arreglos bidimensionales.

public class Ejemplo43 {
    public static void main(String[] args) {
        // int[] arreglo1 = { 1, 2, 3, 4, 5 };
        int[][] arreglo1 = { { 1, 2, 3 }, { 4, 5, 6 } };
        int[][] arreglo2 = { { 1, 2 }, { 3 }, { 4, 5, 6 } };

        System.out.println("Valores en arreglo1 por fila es:");
        imprimirArreglo(arreglo1);

        System.out.println();

        System.out.println("Valores en arreglo2 por fila es:");
        imprimirArreglo(arreglo2);

    }

    public static void imprimirArreglo(int[][] arreglo) {
        for (int fila = 0; fila < arreglo.length; fila++) {
            for (int columna = 0; columna < arreglo[fila].length; columna++)
                System.out.printf("%d  ", arreglo[fila][columna]);
            System.out.println();
        }
    }
}