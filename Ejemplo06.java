
// Fig. 2.15: Comparacion.java
// Compara enteros utilizando instrucciones if, operadores relacionales
// y de igualdad.

import java.util.Scanner;

public class Ejemplo06 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int numero1, numero2;

        System.out.print("Escriba el primer numero: ");
        numero1 = scan.nextInt();

        System.out.print("Escriba el segundo numero: ");
        numero2 = scan.nextInt();
        scan.close();

        if (numero1 == numero2) {
            System.out.printf("%d == %d%n", numero1, numero2);
        }
        if (numero1 != numero2)
            System.out.printf("%d != %d%n", numero1, numero2);

        if (numero1 < numero2)
            System.out.printf("%d < %d%n", numero1, numero2);
        if (numero1 > numero2)
            System.out.printf("%d > %d%n", numero1, numero2);

        if (numero1 <= numero2)
            System.out.printf("%d <= %d%n", numero1, numero2);
        if (numero1 >= numero2)
            System.out.printf("%d >= %d%n", numero1, numero2);
    }
}