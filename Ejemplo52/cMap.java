package Ejemplo52;

import java.util.Scanner;
import java.util.Map;
import java.util.TreeSet;
import java.util.Set;
import java.util.HashMap;

public class cMap {

	public void ejecutarEje() {
		Map<String, Integer> miMap = new HashMap<>();
		crearMap(miMap);
		mostrarMap(miMap);
	}

	private static void crearMap(Map<String, Integer> mapa) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Escriba una cadena: "); // pide la entrada del usuario
		String entrada = scanner.nextLine();

		String[] tokens = entrada.split(" ");

		for (String token : tokens) {
			String color = token.toLowerCase();

			if (mapa.containsKey(color)) { // ¿está la palabra en el mapa?
				int cuenta = mapa.get(color); // obtiene la cuenta actual
				mapa.put(color, cuenta + 1); // incrementa la cuenta
			} else {
				mapa.put(color, 1); // agrega una nueva palabra con una cuenta de 1 al mapav
			}
		}
	}

	private static void mostrarMap(Map<String, Integer> mapa) {
		Set<String> claves = mapa.keySet(); // obtiene las claves
		TreeSet<String> clavesOrdenadas = new TreeSet<>(claves);
		System.out.printf("%nEl mapa contiene:%nClave\t\tValor%n");

		for (String clave : clavesOrdenadas)
			System.out.printf("%-10s%10s%n", clave, mapa.get(clave));

		System.out.printf("%nsize: %d%nisEmpty: %b%n", mapa.size(), mapa.isEmpty());
	}
}