package Ejemplo52;

public class Main {
	public static void main(String[] args) {
		cArrayList arrayList = new cArrayList();
		cLinkedList linkedList = new cLinkedList();
		cOperationList operationList = new cOperationList();
		cStack stack = new cStack();
		cPriorityQueue queue = new cPriorityQueue();
		cSet set = new cSet();
		cMap map = new cMap();
		cProperties prop = new cProperties();

		// arrayList.ejecutarEje();
		// linkedList.ejecutarEje();
		// operationList.ejecutarEje();
		// stack.ejecutarEje();
		// queue.ejecutarEje();
		// set.ejecutarEje();
		// map.ejecutarEje();
		prop.ejecutarEje();
	}
}