package Ejemplo52;

import java.util.HashSet;
import java.util.Set;
import java.util.Collection;
import java.util.List;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.SortedSet;

public class cSet {

	public void ejecutarEje() {
		// crea y muestra un objeto List<String>
		String[] colores = { "rojo", "blanco", "azul", "verde", "gris", "naranja", "carne", "blanco", "cyan", "durazno",
				"gris", "naranja" };
		List<String> lista = Arrays.asList(colores);
		System.out.printf("Lista: %s%n", lista);

		// elimina duplicados y luego imprime los valores únicos
		imprimirSinDuplicados(lista);

		SortedSet<String> arbol = new TreeSet<>(Arrays.asList(colores));

		System.out.print("conjunto ordenado: ");
		imprimirConjunto(arbol);

		// obtiene subconjunto mediante headSet, con base en “naranja”
		System.out.print("headSet (\"naranja\"): ");
		imprimirConjunto(arbol.headSet("naranja"));

		// obtiene subconjunto mediante tailSet, con base en “naranja”
		System.out.print("tailSet (\"naranja\"): ");
		imprimirConjunto(arbol.tailSet("naranja"));

		// obtiene los elementos primero y último
		System.out.printf("primero: %s%n", arbol.first());
		System.out.printf("ultimo : %s%n", arbol.last());
	}

	private static void imprimirSinDuplicados(Collection<String> valores) {
		Set<String> conjunto = new HashSet<>(valores);

		System.out.printf("%nLos valores no duplicados son: ");
		for (String valor : conjunto)
			System.out.printf("%s ", valor);
		System.out.println();
	}

	private static void imprimirConjunto(SortedSet<String> conjunto) {
		for (String s : conjunto)
			System.out.printf("%s ", s);
		System.out.println();
	}
}