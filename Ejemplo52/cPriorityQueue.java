package Ejemplo52;

// Fig. 16.15: PriorityQueueTest.java
// PriorityQueue test program.

import java.util.PriorityQueue;

public class cPriorityQueue {

   public void ejecutarEje() {
      PriorityQueue<Double> queue = new PriorityQueue<>();

      queue.offer(3.2);
      queue.offer(9.8);
      queue.offer(5.4);

      System.out.print("Eliminando los elementos de la cola: ");

      while (queue.size() > 0) {
         System.out.printf("%.1f ", queue.peek());
         queue.poll();
      }
   }
}
