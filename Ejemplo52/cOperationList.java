package Ejemplo52;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class cOperationList {

	public void ejecutarEje() {
		String[] colores = { "Rojo", "Azul", "Verde", "Purpura" };

		// Crea y muestra una list que contiene los elementos del arreglo palos
		List<String> lista = Arrays.asList(colores);

		System.out.printf("Elementos del arreglo desordenados: %s%n", lista);

		Collections.sort(lista); // ordena ArrayList
		System.out.printf("Elementos del arreglo ordenados: %s%n", lista);

		Collections.sort(lista, Collections.reverseOrder());
		System.out.printf("Elementos del arreglo ordenados descendente: %s%n", lista);

		Collections.shuffle(lista);
		System.out.printf("Elementos del arreglo mezclados: %s%n", lista);

		String[] colorescopia = new String[4];

		List<String> copiaLista = Arrays.asList(colorescopia);

		Collections.copy(copiaLista, lista);

		Collections.fill(lista, "Transparente");

		System.out.printf("Elementos del arreglo original: %s%n", lista);

		System.out.printf("Elementos del arreglo copia: %s%n", copiaLista);

		System.out.printf("Max: %s", Collections.max(copiaLista));
		System.out.printf("%nMin: %s%n", Collections.min(copiaLista));

		int resultadoB = 0;
		String clave = "Azul";

		System.out.printf("%nBuscando: %s%n", clave);
		resultadoB = Collections.binarySearch(copiaLista, clave);

		if (resultadoB >= 0)
			System.out.printf("Se encontro en el indice %d%n", resultadoB);
		else
			System.out.printf("No se encontro %d%n", resultadoB);
	}

}