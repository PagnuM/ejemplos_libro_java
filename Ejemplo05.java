
// Fig. 2.7: Suma.java
// Programa que recibe dos números y muestra la suma.
import java.util.Scanner;

public class Ejemplo05 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // int numero1;
        // int numero2;
        // int resultado;
        int numero1, numero2, resultado;

        System.out.print("Escriba el primer numero: ");
        numero1 = scan.nextInt();

        System.out.print("Escriba el segundo numero: ");
        numero2 = scan.nextInt();
        scan.close();

        resultado = numero1 + numero2;

        System.out.println("La suma de los dos numeros es igual a " + resultado);
    }
}