// Fig. 3.1: Cuenta.java
// Clase Cuenta que contiene una variable de instancia llamada nombre
// y métodos para establecer y obtener su valor (Getters y Setters).
package Ejemplo07;

public class Cuenta {
    // Variable de instancia
    private String nombre;

    public void establecerNombre(String nombre) {
        this.nombre = nombre;
    }

    public String obtenerNombre() {
        return this.nombre;
    }

}