import java.util.Scanner;

// Fig. 4.8: PromedioClase.java
// Cómo solucionar el problema del promedio de la clase mediante la repetición controlada por contador.

public class Ejemplo17 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int total = 0;
        int contadorCalificacion = 1;
        while (contadorCalificacion <= 10) {
            System.out.print("Escriba una calificacion: ");
            int calificacion = scan.nextInt();

            total = total + calificacion;
            contadorCalificacion = contadorCalificacion + 1;
        }
        double promedio = total / 10.0;
        System.out.println("El total de la clase es: " + total);
        System.out.println("El promedio es: " + promedio);
    }
}