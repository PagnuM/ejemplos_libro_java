
// Fig. 7.24: ColeccionArrayList.java
// Demostración de la colección de genéricos ArrayList.
import java.util.ArrayList;

public class Ejemplo47 {
    public static void main(String[] args) {
        ArrayList<String> elementos = new ArrayList<String>();

        elementos.add("rojo");
        elementos.add(0, "amarillo");

        mostrar(elementos, "Mostrar elementos de lista: ");

        elementos.add("verde");
        elementos.add("amarillo");
        mostrar(elementos, "Lista con dos elementos nuevos: ");

        elementos.remove("amarillo");
        mostrar(elementos, "Elminar primera instancia de amarillo: ");

        elementos.remove(1);
        mostrar(elementos, "Eliminar segundo de la lista (verde): ");

        System.out.printf("\"azul\" %s esta en la lista%n", elementos.contains("azul") ? "si" : "no");

        System.out.printf("Tamanio de lista: %s%n", elementos.size());
    }

    public static void mostrar(ArrayList<String> elementos, String encabezado) {
        System.out.print(encabezado);

        for (String elemento : elementos)
            System.out.printf(" %s", elemento);
        System.out.println();
    }
}