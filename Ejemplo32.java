// Fig. 6.8: Enum

public class Ejemplo32 {
    private enum Dias {
        DOMINGO, LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO
    };

    public static void main(String[] args) {
        Dias diaActual = Dias.VIERNES;

        switch (diaActual) {
            case LUNES:
                System.out.println("El lunes es el peor dia");
                break;

            case SABADO:
            case DOMINGO:
                System.out.println("Aguante el fin de semana");
                break;

            case VIERNES:
                System.out.println("El viernes es el mejor dia de la semana");
                break;

            default:
                System.out.println("Los dias entre semana son regulares");
                break;
        }
    }
}