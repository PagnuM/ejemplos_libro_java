import java.util.Scanner;

// Fig. 4.10: PromedioClase.java
// Cómo resolver el problema del promedio de una clase mediante la repetición controlada por centinela/bandera.

public class Ejemplo18 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int total = 0;
        int contadorCalificacion = 0;

        System.out.print("Escriba una califiacion o un -1 para terminar: ");
        int califiacion = scan.nextInt();

        while (califiacion != -1) {
            total = total + califiacion;
            contadorCalificacion = contadorCalificacion + 1;

            System.out.print("Escriba una califiacion o un -1 para terminar: ");
            califiacion = scan.nextInt();
        }
        if (contadorCalificacion > 0) {
            double promedio = (double) total / contadorCalificacion;
            System.out.printf("El total es %d\n", total);
            System.out.printf("El promedio de %s calificaciones es %s\n", contadorCalificacion, promedio);
        } else {
            System.out.println("No ha cargado ninguna calificacion, porfavor inicie de nuevo el programa.");
        }
    }
}