// Fig. 3.6: PruebaCuenta.java
// Uso del constructor de Cuenta para inicializar la variable de instancia
package Ejemplo10;

import Ejemplo09.Cuenta;

public class PruebaCuenta {
    public static void main(String[] args) {
        Cuenta cuenta1 = new Cuenta("Juan");
        Cuenta cuenta2 = new Cuenta("Marina");

        System.out.printf("El nombre de la cuenta1 es: %s\n", cuenta1.obtenerNombre());
        System.out.printf("El nombre de la cuenta2 es: %s\n", cuenta2.obtenerNombre());
    }
}