// Fig. 4.15: Incremento.java
// Operadores de preincremento y postincremento.
public class Ejemplo20 {
    public static void main(String[] args) {
        int c = 5;

        // Postincremento
        System.out.printf("Antes del postincremento: c = %d\n", c);
        System.out.printf("Durante del postincremento: c++ = %d\n", c++);
        System.out.printf("Luego del postincremento: c = %d\n", c);

        System.out.println();
        // Preincremento
        c = 5;
        System.out.printf("Antes del preincremento: c = %d\n", c);
        System.out.printf("Durante del preincremento: ++c = %d\n", ++c);
        System.out.printf("Luego del preincremento: c = %d\n", c);
    }
}