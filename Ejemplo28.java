// Fig. 5.13: PruebaBreak.java
// La instrucción break para salir de una instrucción for.

// Fig. 5.14: PruebaContinue.java
// Instrucción continue para terminar una iteración de una instrucción for.

public class Ejemplo28 {
    public static void main(String[] args) {
        int cuenta;
        for (cuenta = 1; cuenta <= 10; cuenta++) {
            if (cuenta == 5)
                continue;
            System.out.printf("%d ", cuenta);
        }
        System.out.println();
    }
}