// Fig. 7.2: InicArreglo.java
// Inicialización de los elementos de un arreglo con valores predeterminados de cero.

public class Ejemplo35 {
    public static void main(String[] args) {
        // tipoDeDato[] nombreArreglo = new tipoDato[cantDeDatos];
        int[] arreglo = new int[10];

        System.out.printf("%s%8s%n", "Indice", "Valor");

        for (int i = 0; i < arreglo.length; i++)
            System.out.printf("%5d%8d%n", i, arreglo[i]);
    }

}