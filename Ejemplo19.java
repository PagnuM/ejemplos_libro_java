import java.util.Scanner;

// Fig. 4.12: Analisis.java
// Analisis de los resultados de un examen, utilizando instrucciones de control anidadas.

public class Ejemplo19 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int aprobados = 0;
        int reprobados = 0;
        int contadorEstudiantes = 1;
        while (contadorEstudiantes <= 10) {
            System.out.print("Inserte el estado del estudiante (1=aprobado/a, 2=reprobado/a): ");
            int estado = scan.nextInt();
            if (estado == 1)
                aprobados = aprobados + 1;
            else
                reprobados = reprobados + 1;
            contadorEstudiantes = contadorEstudiantes + 1;
        }
        System.out.printf("Apropados/as: %d, Reprobados/as: %d\n", aprobados, reprobados);
        if (aprobados > 8)
            System.out.println("Bono para el/la instructora!");
    }
}