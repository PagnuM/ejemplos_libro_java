package Ejemplo51;

import java.util.Scanner;
import java.util.InputMismatchException;

class Main {
  public static void main(String[] args) {
    Scanner entrada = new Scanner(System.in);

    int valor = 0, valor2 = 0;

    try {
      System.out.print("Ingrese un numero entre el siguiente intervalo [1,100]: ");
      valor = entrada.nextInt();
      validarNumero(valor);

      System.out.print("Ingrese otro numero cualquiera: ");
      valor2 = entrada.nextInt();

      System.out.println("La division entre ambos numeros es: " + cociente(valor, valor2));
      // Multi-catch
    } catch (ArithmeticException | InputMismatchException e) {
      System.err.printf("Excepcion: %s%n", e);
      System.out.printf("Valor invalido. %n%n");
      // Catch de excepcion custom
    } catch (MiExcepcion e) {
      System.out.println("MiExcepcion: " + e.getMessage());

    } finally { // Se ejecuta siempre
      System.err.println("Soy un finally");

      // Cerrar stream
      entrada.close();
    }
  }

  // Throw de excepcion custom
  public static void validarNumero(int num) throws MiExcepcion {
    if (num < 1 || num > 100)
      throw new MiExcepcion("Numero fuera de rango");
    System.out.println("El numero " + num + " se encuentra dentro del intervalo.");
  }

  // Throws ArithmeticException
  public static int cociente(int num1, int num2) throws ArithmeticException {
    return num1 / num2;
  }
}