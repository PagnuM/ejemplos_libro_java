// Fig. 4.5: PruebaEstudiante.java
// Crea y prueba objetos Estudiante.
package Ejemplo16;

import Ejemplo15.Estudiante;

public class PruebaEstudiante {
    public static void main(String[] args) {
        Estudiante estudiante1 = new Estudiante("Juan", 5000);
        Estudiante estudiante2 = new Estudiante("Marina", -10);

        System.out.printf("El/La estudiante %s obtuvo como calificacion un %s\n", estudiante1.getNombre(),
                estudiante1.obtenerCalificacion());
        System.out.printf("El/La estudiante %s obtuvo como calificacion un %s\n", estudiante2.getNombre(),
                estudiante2.obtenerCalificacion());
    }
}