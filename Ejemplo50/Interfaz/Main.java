package Ejemplo50.Interfaz;

public class Main {
    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        // System.out.printf("8 + 2 = %d\n", calc.suma(8, 2));
        // System.out.printf("8 - 2 = %d\n", calc.resta(8, 2));
        // System.out.printf("8 * 2 = %d\n", calc.multiplicacion(8, 2));
        // System.out.printf("8 / 2 = %d\n", calc.division(8, 2));

        System.out.println("\nICalculadora[]: ");

        ICalculadora[] calculadoras = { calc, calc, calc };

        for (ICalculadora calculadora : calculadoras) {
            System.out.printf("8 + 2 = %d\n", calculadora.suma(8, 2));
            System.out.printf("8 - 2 = %d\n", calculadora.resta(8, 2));
            System.out.printf("8 * 2 = %d\n", calculadora.multiplicacion(8, 2));
            System.out.printf("8 / 2 = %d\n\n", calculadora.division(8, 2));
        }
    }
}