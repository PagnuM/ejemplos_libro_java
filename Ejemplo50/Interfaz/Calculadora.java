package Ejemplo50.Interfaz;

public class Calculadora implements ICalculadora {
    @Override
    public int suma(int a, int b) {
        return a + b;
    }

    @Override
    public int resta(int a, int b) {
        return a - b;
    }

    @Override
    public int multiplicacion(int a, int b) {
        return a * b;
    }

    @Override
    public int division(int a, int b) {
        return a / b;
    }

    public int potencia(int a, int b) {
        return a ^ b;
    }
}