package Ejemplo50.Polimorfismo;

public class BDiamante extends Cubo {

	public BDiamante() {
		super(4, "bloque de diamante", "Bdiamante.png");
	}

	@Override
	public void destruir() {
		System.out.println("Me destruyeron! Ten tu diamante.");
	}

	@Override
	public void colocar() {
		System.out.println("Que buen momento para admirar mi diamante!");
	}
}
