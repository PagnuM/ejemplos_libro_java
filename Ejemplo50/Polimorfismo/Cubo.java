package Ejemplo50.Polimorfismo;

public abstract class Cubo {
	protected int dimension;
	protected String nombre;
	protected String textura;

	public Cubo(int dimension, String nombre, String textura) {
		this.dimension = dimension;
		this.nombre = nombre;
		this.textura = textura;
	}

	public void destruir() {
		System.out.println("Me destruyeron!");
	}

	public abstract void colocar();
}