package Ejemplo50.Polimorfismo;

class Main {
	public static void main(String[] args) {
		BCarbon bc1 = new BCarbon();
		BDiamante bd1 = new BDiamante();

		Cubo[] cubos = { bc1, bd1 };

		for (Cubo cubo : cubos) {
			System.out.println("\nTe encuentras un " + cubo.nombre + " y lo rompes: ");
			cubo.destruir();
			System.out.println("\nColocas tu querido " + cubo.nombre + ": ");
			cubo.colocar();
		}
	}
}