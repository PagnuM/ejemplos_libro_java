package Ejemplo50.Polimorfismo;

public class BCarbon extends Cubo {

    public BCarbon() {
        super(4, "bloque de carbon", "Bcarbon.png");
    }

    @Override
    public void destruir() {
        System.out.println("Me destruyeron! Ten un carbon.");
    }

    @Override
    public void colocar() {
        System.out.println("Que buen momento para cocinar algo con mi carbon.");
    }

}