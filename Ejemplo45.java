// Fig. 7.21: InicArreglo.java
// Uso de los argumentos de línea de comandos para inicializar un arreglo.

public class Ejemplo45 {
    public static void main(String[] args) {

        if (args.length != 3) {
            System.out.println("Error: Vuelva a escribir el comando correctamente.");
        } else {
            int longitudArreglo = Integer.parseInt(args[0]);
            int[] arreglo = new int[longitudArreglo];

            int valorInicial = Integer.parseInt(args[1]);
            int incremento = Integer.parseInt(args[2]);

            for (int i = 0; i < arreglo.length; i++)
                arreglo[i] = valorInicial + incremento * i;

            System.out.printf("%s%8s%n", "Indice", "Valor");
            for (int i = 0; i < arreglo.length; i++) {
                System.out.printf("%5d%8d%n", i, arreglo[i]);
            }
        }
    }
}

// java Ejemplo45 4 0 2
/*
 * [0] = 0, [1] = 2, [2] = 4, [3] = 6
 */