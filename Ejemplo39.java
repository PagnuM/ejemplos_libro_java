import java.security.SecureRandom;

// Fig. 7.7: TirarDado.java
// Programa para tirar dados que utiliza arreglos en vez de switch.

public class Ejemplo39 {
    public static void main(String[] args) {
        SecureRandom numAleatorios = new SecureRandom();

        final int LADOS = 20;
        int[] frecuencia = new int[LADOS];

        for (int i = 0; i < 6000000; i++)
            ++frecuencia[numAleatorios.nextInt(LADOS)];

        System.out.printf("%s%10s%n", "Cara", "Frecuencia");

        for (int i = 0; i < frecuencia.length; i++)
            System.out.printf("%4d%10d%n", i + 1, frecuencia[i]);
    }
}