import java.util.Scanner;

// Fig. 5.9: CalificacionesLetra.java
// La clase CalificacionesLetra usa la instrucción switch para contar las 
// calificaciones de letra.

public class Ejemplo27 {
    public static void main(String[] args) {
        int total = 0;
        int contadorCalif = 0;
        int aContar = 0;
        int bContar = 0;
        int cContar = 0;
        int dContar = 0;
        int fContar = 0;

        Scanner entrada = new Scanner(System.in);
        System.out.println("Introduzca las calificacions en el ragno de 0-100");
        System.out.println("Al terminar de cargar, presione el comando para terminar");

        while (entrada.hasNext()) {
            int calificacion = entrada.nextInt();
            total += calificacion;
            contadorCalif++;

            switch (calificacion / 10) {
                case 9:
                case 10:
                    ++aContar;
                    break;
                case 8:
                    ++bContar;
                    break;
                case 7:
                    ++cContar;
                    break;
                case 6:
                    ++dContar;
                    break;
                default:
                    ++fContar;
                    break;
            }
        }
        System.out.printf("%nReporte de calificaciones:%n");

        if (contadorCalif != 0) {
            double promedio = (double) total / contadorCalif;
            System.out.println("El total de las " + contadorCalif + " calificaciones es: " + total);
            System.out.printf("El promedio de la clase es %.2f%n", promedio);
            System.out.println("Numero de estudiantes que recibieron cada calificacion:");
            System.out.printf("A: %d%n", aContar);
            System.out.printf("B: %d%n", bContar);
            System.out.printf("C: %d%n", cContar);
            System.out.printf("D: %d%n", dContar);
            System.out.printf("F: %d%n", fContar);
        } else {
            System.out.println("No se han introducido calificaciones.");
        }
    }
}
