// Fig. 3.5: Cuenta.java
// Clase Cuenta con un constructor que inicializa el nombre.
package Ejemplo09;

public class Cuenta {
    // Variable de instancia
    private String nombre;

    public Cuenta(String nombre) {
        establecerNombre(nombre);
    }

    // Setter
    public void establecerNombre(String nombre) {
        this.nombre = nombre;
    }

    // Getter
    public String obtenerNombre() {
        return this.nombre;
    }

}