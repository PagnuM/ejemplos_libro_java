// Fig. 5.2: ContadorFor.java
// Repetición controlada con contador, con la instrucción de repetición for.

public class Ejemplo23 {
    public static void main(String[] args) {

        for (int contador = 1; contador <= 10; contador++)
            System.out.printf("%d ", contador);
        System.out.println();
    }
}