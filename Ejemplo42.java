// Fig. 7.13: PasoArreglo.java
// Paso de arreglos y elementos individuales de un arreglo a los métodos.

public class Ejemplo42 {
    public static void main(String[] args) {
        int[] arreglo = { 1, 2, 3, 4, 5 };

        System.out.println("Efectos de pasar una referencia a un arreglo completo:");
        System.out.println("Los valores del arreglo original son:");

        for (int num : arreglo)
            System.out.print("\t" + num);
        System.out.println();

        modificarArreglo(arreglo);

        System.out.println("Los valores del arreglo modificado son:");
        for (int num : arreglo)
            System.out.print("\t" + num);
        System.out.println();

        System.out.println("Efectos de pasar el valor de un elemento del arreglo:");
        System.out.println("arreglo[3] antes de modificarElemento: " + arreglo[3]);

        modificarElemento(arreglo[3]);

        System.out.println("arreglo[3] despues de modificarElemento: " + arreglo[3]);
    }

    public static void modificarArreglo(int[] arregloMetodo) {
        for (int i = 0; i < arregloMetodo.length; i++)
            arregloMetodo[i] *= 2;
    }

    public static void modificarElemento(int elemento) {
        elemento *= 2;
        System.out.println("Valor del elemento en modificarElemento: " + elemento);
    }
}