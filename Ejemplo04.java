// Fig. 2.6: Bienvenido4.java
// Imprimir varias líneas con el método System.out.printf.
public class Ejemplo04 {
    public static void main(String[] args) {
        System.out.printf("%s%n%s%n", "Bienvenid@ a la ", "programacion en Java!");
    }
}