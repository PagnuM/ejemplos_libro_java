import java.security.SecureRandom;

// Fig. 6.7: TirarDado.java
// Tirar un dado de seis lados 6,000,000 veces.

public class Ejemplo31 {
    public static void main(String[] args) {
        SecureRandom numeroAleatorio = new SecureRandom();

        int frecuencia1 = 0;
        int frecuencia2 = 0;
        int frecuencia3 = 0;
        int frecuencia4 = 0;
        int frecuencia5 = 0;
        int frecuencia6 = 0;

        for (int i = 1; i <= 6000000; i++) {
            int cara = 1 + numeroAleatorio.nextInt(6);

            switch (cara) {
                case 1:
                    frecuencia1++;
                    break;

                case 2:
                    frecuencia2++;
                    break;

                case 3:
                    frecuencia3++;
                    break;

                case 4:
                    frecuencia4++;
                    break;

                case 5:
                    frecuencia5++;
                    break;

                default:
                    frecuencia6++;
                    break;
            }
        }
        System.out.println("Cara\tFrecuencia");
        System.out.println("1)\t" + frecuencia1);
        System.out.println("2)\t" + frecuencia2);
        System.out.println("3)\t" + frecuencia3);
        System.out.println("4)\t" + frecuencia4);
        System.out.println("5)\t" + frecuencia5);
        System.out.println("6)\t" + frecuencia6);
    }
}