// Fig. 3.8: Cuenta.java
// La clase Cuenta con una variable de instancia double llamada saldo y un constructor
// además de un método llamado deposito que realiza validación.
package Ejemplo11;

public class Cuenta {
    // Variable de instancia
    private String nombre;
    private double saldo;

    public Cuenta(String nombre, double saldo) {
        establecerNombre(nombre);
        depositar(saldo);
    }

    // Getter
    public String obtenerNombre() {
        return this.nombre;
    }

    public double obtenerSaldo() {
        return this.saldo;
    }

    // Setter
    public void establecerNombre(String nombre) {
        this.nombre = nombre;
    }

    public void depositar(double monto) {
        if (monto > 0)
            saldo = saldo + monto;
    }

}