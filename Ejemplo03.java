// Fig. 2.4: Bienvenido3.java
// Imprimir varias líneas de texto con una sola instrucción.
public class Ejemplo03 {
    public static void main(String[] args) {
        System.out.println("Bienvenid@ a\nla programacion\nen Java!");
    }
}