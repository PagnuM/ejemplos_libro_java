// Fig. 7.8: EncuestaEstudiantes.java
// Programa de análisis de una encuesta.

public class Ejemplo40 {
    public static void main(String[] args) {
        int[] respuestas = { 1, 2, 5, 4, 3, 5, 2, 1, 3, 3, 1, 14 };
        int[] frecuencia = new int[5];

        for (int i = 0; i < respuestas.length; i++) {
            try {
                ++frecuencia[respuestas[i] - 1];
            } catch (Exception e) {
                System.out.println("Stack de excepcion");
                System.out.println(e);

                System.out.printf("respuestas[%d] = %d%n%n", i, respuestas[i]);
            }
        }

        System.out.printf("%s%10s%n", "Clasificacion", "Frecuencia");

        for (int i = 0; i < frecuencia.length; i++) {
            System.out.printf("%6d%10d%n", i + 1, frecuencia[i]);
        }
    }
}