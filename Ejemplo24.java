// Fig. 5.5: Suma.java
// Sumar enteros con la instrucción for.

public class Ejemplo24 {
    public static void main(String[] args) {
        int total = 0;
        for (int contador = 2; contador <= 20; contador += 2)
            total += contador;
        System.out.println("Total: " + total);
    }
}