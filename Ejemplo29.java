import java.util.Scanner;

// Fig. 6.3: BuscadorMaximo.java
// Método maximo, declarado por el programador, con tres parámetros double.

public class Ejemplo29 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Escriba tres numeros de punto flotante separados por espacios: ");
        double num1 = entrada.nextDouble();
        double num2 = entrada.nextDouble();
        double num3 = entrada.nextDouble();
        entrada.close();

        double max = maximo(num1, num2, num3);
        System.out.println("El numero maximo entre los tres numeros es: " + max);

    }

    public static double maximo(double num1, double num2, double num3) {
        double retorno = num1;

        if (num2 > retorno)
            retorno = num2;

        if (num3 > retorno)
            retorno = num3;

        return retorno;
    }

}