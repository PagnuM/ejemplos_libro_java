// Fig. 7.5: SumaArreglo.java
// Cálculo de la suma de los elementos de un arreglo.

public class Ejemplo38 {
    public static void main(String[] args) {
        int[] arreglo = { 47, 12, 74, 62, 35, 16, 37, 34, 47, 19 };
        int total = 0;

        for (int i = 0; i < arreglo.length; i++)
            total += arreglo[i];
        System.out.println("El total de la suma de los elementos del arreglo es: " + total);
        System.out.println("El promedio de los elementos del arreglo es: " + (total / (float) arreglo.length));
    }
}