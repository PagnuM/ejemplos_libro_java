// Fig. 5.6: Interes.java
// Cálculo del interés compuesto con for.

public class Ejemplo25 {
    public static void main(String[] args) {
        double monto;
        double principal = 1000;
        double tasa = 1.05;

        System.out.printf("%s%20s%n", "Anio", "Monto en deposito");

        for (int anio = 1; anio <= 10; ++anio) {
            monto = principal * Math.pow(tasa, anio);

            System.out.printf("%4d%,20.2f%n", anio, monto);
        }
    }
}