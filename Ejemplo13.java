
// Fig. 3.12: Dialogo1.java
// Uso de JOptionPane para mostrar varias líneas en un cuadro de diálogo.
import javax.swing.JOptionPane;

public class Ejemplo13 {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Hola mundo desde java swing");
    }
}