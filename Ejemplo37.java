// Fig. 7.4: InicArreglo.java
// Cálculo de los valores a colocar en los elementos de un arreglo.

public class Ejemplo37 {
    public static void main(String[] args) {
        // tipoDeDato[] nombreArreglo = new tipoDato[cantDeDatos];
        int[] arreglo = new int[10];

        for (int i = 0; i < arreglo.length; i++)
            arreglo[i] = 2 + 2 * i;

        System.out.printf("%s%8s%n", "Indice", "Valor");

        for (int i = 0; i < arreglo.length; i++)
            System.out.printf("%5d%8d%n", i, arreglo[i]);
    }

}