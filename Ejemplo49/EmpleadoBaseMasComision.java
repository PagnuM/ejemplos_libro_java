package Ejemplo49;

// Subclase
public class EmpleadoBaseMasComision extends EmpleadoComisionista {
   // variable de instancia privada
   private double salarioBase;

   public EmpleadoBaseMasComision(String nombre, String apellido, String numeroSeguridadSocial, double ventasBrutas,
         double porcentajeComision, double salarioBase) {
      super(nombre, apellido, numeroSeguridadSocial, ventasBrutas, porcentajeComision);
      try {
         setSalarioBase(salarioBase);
      } catch (IllegalArgumentException e) {
         throw e;
      }
   }

   // Throws IllegalArgumentException
   public void setSalarioBase(double salarioBase) {
      if (salarioBase < 0.0)
         throw new IllegalArgumentException("El salario base debe ser >= 0.0");

      this.salarioBase = salarioBase;
   }

   public double getSalarioBase() {
      return salarioBase;
   }

   @Override
   public double calcularIngreso() {
      return getSalarioBase() + super.calcularIngreso();
   }

   @Override
   public String toString() {
      return String.format("%s%nSalario Base: %.2f", super.toString(), getSalarioBase());
   }
}
