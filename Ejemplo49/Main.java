package Ejemplo49;

public class Main {
      public static void main(String[] args) {
            EmpleadoBaseMasComision empleado = new EmpleadoBaseMasComision("Bob", "Lewis", "333-33-3333", 5000, 0.04,
                        300);

            System.out.println("Informacion de los empleados obtenida por los getters:");
            System.out.printf("Nombre: %s%n", empleado.getNombre());
            System.out.printf("Apellido: %s%n", empleado.getApellido());
            System.out.printf("Numero de seguro social: %s%n", empleado.getNumeroSeguridadSocial());
            System.out.printf("Ventas brutas: %s%n", empleado.getVentasBrutas());
            System.out.printf("Porcentaje de comision: %s%n", empleado.getPorcentajeComision());
            System.out.printf("Salario base: %s%n", empleado.getSalarioBase());

            try {
                  empleado.setSalarioBase(-1);
            } catch (IllegalArgumentException e) {
                  System.out.println("Excepcional!: " + e.getMessage());
            }

            System.out.println(
                        "\nInformacion actualizada de empleado obtenida a traves del metodo toString:\n" + empleado);
      }
}
