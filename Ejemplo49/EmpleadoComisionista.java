package Ejemplo49;

// Superclase
public class EmpleadoComisionista {
   // Variables de instancia de tipo protected
   protected final String nombre;
   protected final String apellido;
   protected final String numeroSeguridadSocial;
   protected double ventasBrutas; // ventas brutas semanales
   protected double porcentajeComision; // procentaje de comision

   public EmpleadoComisionista(String nombre, String apellido, String numeroSeguridadSocial, double ventasBrutas,
         double porcentajeComision) throws IllegalArgumentException {
      try {
         setVentasBrutas(ventasBrutas);
         setPorcentajeComision(porcentajeComision);
         this.nombre = nombre;
         this.apellido = apellido;
         this.numeroSeguridadSocial = numeroSeguridadSocial;
      } catch (IllegalArgumentException e) {
         throw e;
      }
   }

   public String getNombre() {
      return nombre;
   }

   public String getApellido() {
      return apellido;
   }

   public String getNumeroSeguridadSocial() {
      return numeroSeguridadSocial;
   }

   public double getVentasBrutas() {
      return ventasBrutas;
   }

   public double getPorcentajeComision() {
      return porcentajeComision;
   }

   // Throws IllegalArgumentException
   public void setVentasBrutas(double ventasBrutas) throws IllegalArgumentException {
      if (ventasBrutas < 0.0)
         throw new IllegalArgumentException("Las ventas brutas deben ser >= 0.0");

      this.ventasBrutas = ventasBrutas;
   }

   // Throws IllegalArgumentException
   public void setPorcentajeComision(double porcentajeComision) {
      if (porcentajeComision <= 0.0 || porcentajeComision >= 1.0)
         throw new IllegalArgumentException("El procentaje de comsion debe ser > 0.0 y < 1.0");

      this.porcentajeComision = porcentajeComision;
   }

   public double calcularIngreso() {
      return getPorcentajeComision() * getVentasBrutas();
   }

   @Override
   public String toString() {
      return String.format(
            "Empleado: %s %s%nNumero de seguro social: %s%nVentasBrutas: %.2f%nPorcentaje de comision: %.2f",
            getNombre(), getApellido(), getNumeroSeguridadSocial(), getVentasBrutas(), getPorcentajeComision());
   }
}