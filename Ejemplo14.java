
// Fig. 3.13: DialogoNombre.java
// Entrada básica con un cuadro de diálogo.
import javax.swing.JOptionPane;

public class Ejemplo14 {
    public static void main(String[] args) {
        String nombre = JOptionPane.showInputDialog("Cual es su nombre?");

        String text = String.format("Bienvenid@ %s al mundo de la programacion en Java", nombre);

        JOptionPane.showMessageDialog(null, text);
    }
}