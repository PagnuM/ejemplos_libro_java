package Ejemplo53;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EmpleadoEJ {

	public void ejecutarEje() {

		Empleado[] empleados = { new Empleado("Jason", "Red", 5000, "TI"), new Empleado("Ashley", "Green", 6000, "TI"),
				new Empleado("Matthew", "Indigo", 10000, "Ventas"), new Empleado("James", "Indigo", 1000, "Marketing"),
				new Empleado("Luke", "Indigo", 3500, "TI"), new Empleado("Jason", "Blue", 6750, "Ventas"),
				new Empleado("Wendy", "Brown", 3333, "Marketing") };

		List<Empleado> lista = Arrays.asList(empleados);

		System.out.println("Lista completa de empleados:");
		lista.stream().forEach(System.out::println);
		System.out.println("-------------------------");

		Function<Empleado, String> porPrimerNombre = Empleado::obtenerPrimerNombre;
		Function<Empleado, String> porApellidoPaterno = Empleado::obtenerApellidoPaterno;

		Comparator<Empleado> apellidoLuegoNombre = Comparator.comparing(porApellidoPaterno)
				.thenComparing(porPrimerNombre);

		System.out.printf("%nEmpleados en orden ascendente por apellido y luego por nombre:%n");
		lista.stream().sorted(apellidoLuegoNombre).forEach(System.out::println);

		System.out.printf("%nEmpleados en orden descendente por apellido y luego por nombre:%n");
		lista.stream().sorted(apellidoLuegoNombre.reversed()).forEach(System.out::println);

		System.out.println("-------------------------");

		Predicate<Empleado> cuatroASeisMil = e2 -> (e2.obtenerSalario() >= 4000 && e2.obtenerSalario() <= 6000);

		// Muestra los empleados con salarios en el rango $4000-$6000
		// en orden ascendente por salario
		System.out.printf("%nEmpleados que ganan $4000-$6000 mensuales ordenados por salario:%n");
		lista.stream().filter(cuatroASeisMil).sorted(Comparator.comparing(Empleado::obtenerSalario))
				.forEach(System.out::println);

		// Muestra el primer empleado con salario en el rango $4000-$6000
		System.out.printf("%nPrimer empleado que gana $4000-$6000:%n%s%n",
				lista.stream().filter(cuatroASeisMil).findFirst().get());

		System.out.println("-------------------------");

		System.out.printf("%nApellidos de empleados unicos:%n");
		lista.stream().map(Empleado::obtenerApellidoPaterno).distinct().sorted().forEach(System.out::println);

		// muestra sólo nombre y apellido
		System.out.printf("%nNombres de empleados en orden por apellido y luego por nombre:%n");
		lista.stream().sorted(apellidoLuegoNombre).map(Empleado::obtenerPrimerNombre).forEach(System.out::println);

		System.out.println("-------------------------");

		System.out.printf("%nEmpleados por departamento:%n");

		Map<String, List<Empleado>> agrupadoPorDepartamento = lista.stream()
				.collect(Collectors.groupingBy(Empleado::obtenerDepartamento));

		agrupadoPorDepartamento.forEach((departamento, empleadosEnDepartamento) -> {
			System.out.println(departamento);
			empleadosEnDepartamento.forEach(empleado -> System.out.printf(" %s%n", empleado));
		});

		System.out.println("-------------------------");

		System.out.printf("%nConteo de empleados por departamento:%n");
		Map<String, Long> conteoEmpleadosPorDepartamento = lista.stream()
				.collect(Collectors.groupingBy(Empleado::obtenerDepartamento, TreeMap::new, Collectors.counting()));

		conteoEmpleadosPorDepartamento.forEach(
				(departamento, conteo) -> System.out.printf("%s tiene %d empleado(s)%n", departamento, conteo));

		System.out.println("-------------------------");

		System.out.printf("%nSuma de los salarios de los empleados (mediante el metodo sum): %.2f%n",
				lista.stream().mapToDouble(Empleado::obtenerSalario).sum());

		// calcula la suma de los salarios de los empleados con el método reduce de
		// Stream
		System.out.printf("Suma de los salarios de los empleados (mediante el metodo reduce): %.2f%n",
				lista.stream().mapToDouble(Empleado::obtenerSalario).reduce(0, (valor1, valor2) -> valor1 + valor2));

		System.out.printf("Promedio de salarios de los empleados: %.2f%n",
				lista.stream().mapToDouble(Empleado::obtenerSalario).average().getAsDouble());
	}
}