package Ejemplo53;

import java.security.SecureRandom;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FlujosAleatorios {

    public void ejecutarEje() {
        SecureRandom aleatorio = new SecureRandom();

        // tira un dado 1,000 de veces y sintetiza los resultados
        System.out.printf("%-6s%s%n", "Cara", "Frecuencia");
        aleatorio.ints(1000000, 1, 7).boxed().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .forEach((cara, frecuencia) -> System.out.printf("%-6d%d%n", cara, frecuencia));
    }
} // fin de la clase
