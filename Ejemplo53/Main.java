package Ejemplo53;

public class Main {

	// Lambda:
	// (listaParámetros) -> {instrucciones}

	public static void main(String[] args) {
		StreamString colores = new StreamString();
		StreamNumeros nums = new StreamNumeros();
		EmpleadoEJ empleadoej = new EmpleadoEJ();
		FlujosAleatorios flujos = new FlujosAleatorios();

		// colores.ejecutarEje();
		// nums.ejecutarEje();
		// empleadoej.ejecutarEje();
		flujos.ejecutarEje();
	}
}