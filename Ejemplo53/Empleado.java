package Ejemplo53;

public class Empleado {

	private String primerNombre;
	private String apellidoPaterno;
	private double salario;
	private String departamento;

	public Empleado(String primerNombre, String apellidoPaterno, double salario, String departamento) {
		this.primerNombre = primerNombre;
		this.apellidoPaterno = apellidoPaterno;
		this.salario = salario;
		this.departamento = departamento;
	}

	public void establecerPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String obtenerPrimerNombre() {
		return primerNombre;
	}

	public void establecerApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String obtenerApellidoPaterno() {
		return apellidoPaterno;
	}

	public void establecerSalario(double salario) {
		this.salario = salario;
	}

	public double obtenerSalario() {
		return salario;
	}

	public void establecerDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String obtenerDepartamento() {
		return departamento;
	}

	public String obtenerNombre() {
		return String.format("%s %s", obtenerPrimerNombre(), obtenerApellidoPaterno());
	}

	@Override
	public String toString() {
		return String.format("%-8s %-8s", obtenerPrimerNombre(), obtenerApellidoPaterno(), obtenerSalario(),
				obtenerDepartamento());

	}

}