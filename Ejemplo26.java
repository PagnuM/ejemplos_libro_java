// Fig. 5.7: PruebaDoWhile.java
// La instrucción de repetición do...while.

public class Ejemplo26 {
    public static void main(String[] args) {
        int contador = 1;
        do {
            System.out.printf("%d ", contador++);
        } while (contador <= 10);
        System.out.println();
    }
}