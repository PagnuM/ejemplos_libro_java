// Fig. 6.10: SobrecargaMetodos.java
// Declaraciones de métodos sobrecargados.

public class Ejemplo34 {
    public static void main(String[] args) {
        System.out.println("El cuadrado de 2 es: " + cuadrado(2));
        System.out.println("El cuadrado de 2.6 es: " + cuadrado(2.6));
    }

    public static int cuadrado(int a) {
        return a * a;
    }

    public static double cuadrado(double a) {
        return a * a;
    }
}