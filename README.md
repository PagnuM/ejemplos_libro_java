Ejemplos del libro "Como programar en Java, 10ma edicion - Paul Deitel"
-----------------------------------------------------------------------

La versión de Java utilizada es la 8.

Se uso Visual Studio Code para programar todos los ejemplos, y se puede descargar por acá: https://vscodium.com/

Podes seguir mis tutoriales en: https://www.youtube.com/playlist?list=PL34myJlXgDtKgWarMAkTWTAjaXzLAnxfS
