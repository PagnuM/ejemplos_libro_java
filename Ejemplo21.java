import java.awt.Graphics;

import javax.swing.JPanel;

// Fig. 4.18: PanelDibujo.java
// Uso de drawLine para conectar las esquinas de un panel.

public class Ejemplo21 extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        int anchura = getWidth();
        int altura = getHeight();

        g.drawLine(0, 0, anchura, altura);
        g.drawLine(anchura, 0, 0, altura);
    }
}