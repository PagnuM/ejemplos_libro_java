// Fig. 7.3: InicArreglo.java
// Inicialización de los elementos de un arreglo con un inicializador de arreglo.

public class Ejemplo36 {
    public static void main(String[] args) {
        // tipoDeDato[] nombreArreglo = new tipoDato[cantDeDatos];
        int[] arreglo = { 90, 8, 7, 6, 58, 4, 3, 2, 1, 0 };

        System.out.printf("%s%8s%n", "Indice", "Valor");

        for (int i = 0; i < arreglo.length; i++)
            System.out.printf("%5d%8d%n", i, arreglo[i]);
    }

}