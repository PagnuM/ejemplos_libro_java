// Fig. 6.9: Alcance.java
// La clase Alcance demuestra los alcances de los campos y las variables locales.

public class Ejemplo33 {
    private static int x = 1;

    public static void main(String[] args) {
        int x = 5;
        System.out.println("La x en el main es igual a " + x);

        usarVariableLocal();
        usarCampo();
        usarVariableLocal();
        usarCampo();

        System.out.println("\nLa x en el main es igual a " + x);
    }

    public static void usarVariableLocal() {
        int x = 25;
        System.out.println("\nLa x local al entrar al metodo usarVariableLocal es " + x);
        ++x;
        System.out.println("La x local antes de salir del metodo usarVariableLocal es " + x);
    }

    public static void usarCampo() {
        System.out.println("\nEl campo x al entrar al metodo usarCampo es " + x);
        x *= 10;
        System.out.println("El campo x antes de salir del metodo usarCampo es " + x);
    }
}