// Fig. 4.4: Estudiante.java
// Clase Estudiante que almacena el nombre y promedio de un estudiante.
package Ejemplo15;

public class Estudiante {

    private String nombre;
    private double promedio;

    // Constructor
    public Estudiante(String nombre, double promedio) {
        setNombre(nombre);
        setPromedio(promedio);
    }

    // Getters
    public String getNombre() {
        return nombre;
    }

    public double getPromedio() {
        return promedio;
    }

    // Setters
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPromedio(double promedio) {
        // Esto es un if anidado
        if (promedio > 0)
            if (promedio <= 100)
                this.promedio = promedio;
    }

    // Otros metodos
    public String obtenerCalificacion() {
        // If-Else
        if (promedio >= 90)
            return "A";
        // Else-if
        else if (promedio >= 80)
            return "B";
        else if (promedio >= 70)
            return "C";
        else if (promedio >= 60)
            return "D";
        else
            return "F";
    }
}