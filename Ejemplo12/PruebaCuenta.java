// Fig. 3.9: PruebaCuenta.java
// Entrada y salida de números de punto flotante con objetos Cuenta.
package Ejemplo12;

import java.util.Scanner;
import Ejemplo11.Cuenta;

public class PruebaCuenta {
    public static void main(String[] args) {
        Cuenta cuenta1 = new Cuenta("Juan", 200.509);
        Cuenta cuenta2 = new Cuenta("Marina", -100.0277);

        System.out.printf("%s tiene %.2f en su cuenta\n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("%s tiene %.2f en su cuenta\n\n", cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());

        Scanner scan = new Scanner(System.in);
        System.out.print("Ingrese el monto a depositar a la cuenta 1: ");
        cuenta1.depositar(scan.nextDouble());
        System.out.printf("El nuevo monto a sido agregado a la cuenta de %s\n\n", cuenta1.obtenerNombre());

        System.out.printf("%s tiene %.2f en su cuenta\n", cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
        System.out.printf("%s tiene %.2f en su cuenta\n", cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
    }
}