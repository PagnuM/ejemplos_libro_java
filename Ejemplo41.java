// Fig. 7.12: PruebaForMejorado.java
// Uso de la instrucción for mejorada para sumar el total de enteros en un arreglo.

public class Ejemplo41 {
    public static void main(String[] args) {
        // for (Tipo variable : coleccion) {
        // }
        int[] arreglo = { 47, 17, 38, 58, 127, 8 };
        int total = 0;

        for (int numActual : arreglo)
            total += numActual;

        System.out.println("El total de la suma de los elementos es igual a: " + total);
    }
}