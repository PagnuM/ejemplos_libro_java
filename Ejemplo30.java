import java.security.SecureRandom;

// Fig. 6.6: EnterosAleatorios.java
// Enteros aleatorios desplazados y escalados.

public class Ejemplo30 {
    public static void main(String[] args) {
        SecureRandom numeroAleatorio = new SecureRandom();

        for (int i = 1; i <= 20; i++) {
            int cara = 1 + numeroAleatorio.nextInt(6);

            System.out.print(cara + " ");

            if (i % 5 == 0)
                System.out.println();
        }
    }
}