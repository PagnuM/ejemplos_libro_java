package Ejemplo48;

public enum Libro {
    // Enums constantes con parametros
    JHTP("Java How to Program", "2015"), CHTP("Java How to Program", "2015"),
    IW3HTP("Internet & World Wide Web How to Program", "2012"), CPPHTP("C+++ How to Program", "2014");

    private final String titulo;
    private final String anioCopyright;

    Libro(String titulo, String anioCopyright) {
        this.titulo = titulo;
        this.anioCopyright = anioCopyright;
    }

    public String obtenerTitulo() {
        return titulo;
    }

    public String obtenerAnioCopyright() {
        return anioCopyright;
    }
}