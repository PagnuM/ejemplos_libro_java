package Ejemplo48;

// Declaracion de tipo static
import static java.lang.Math.*;
import java.math.BigDecimal;
import java.text.NumberFormat;

public class Ejemplo48 {
    public static void main(String[] args) {

        System.out.println("EXCEPCIONES\n-----------");
        // Excepciones
        Tiempo t1;
        try {
            t1 = new Tiempo(23, 29, 10);
            System.out.println("t1 = " + t1);
        } catch (Exception e) {
            System.out.println("Excepcion en t1: " + e.getMessage());
        }

        System.out.println();

        Tiempo t2;
        try {
            t2 = new Tiempo(10);
            System.out.println("t2 = " + t2);
        } catch (Exception e) {
            System.out.println("Excepcion en t2: " + e.getMessage());
        }

        System.out.println("Cantidad de instancias de tiempo es: " + Tiempo.getCantidadInstancias());

        System.out.println("\nIMPORT ESTATICO\n---------------");
        // Usando importacion estatica de Math
        System.out.println("El cuadrado de 2 es: " + pow(2, 2));

        System.out.println("\nENUM\n----");
        // Enums
        System.out.println("Todos los libros:");
        for (Libro libro : Libro.values())
            System.out.printf("%-10s%-45s%s%n", libro, libro.obtenerTitulo(), libro.obtenerAnioCopyright());

        System.out.println("\nBIG DECIMAL\n-----------");
        // Big Decimal
        BigDecimal inicial = BigDecimal.valueOf(1000.0);
        BigDecimal tasa = BigDecimal.valueOf(0.05);

        System.out.printf("%s%20s%n", "Anio", "Monto en deposito");

        for (int anio = 1; anio <= 10; ++anio) {
            BigDecimal monto = inicial.multiply(tasa.add(BigDecimal.ONE).pow(anio));
            System.out.printf("%4d%20s%n", anio, NumberFormat.getCurrencyInstance().format(monto));
        }
    }
}