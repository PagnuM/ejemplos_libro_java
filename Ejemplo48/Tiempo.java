package Ejemplo48;

public class Tiempo {
    // Variable estatica
    private static int cantidadInstancias = 0;
    private int hora;
    private int minuto;
    private int segundo;

    // demostracion throw y de throws
    public void establecerTiempo(int hora, int minuto, int segundo) throws IllegalArgumentException {
        if (hora < 0 || hora >= 24)
            throw new IllegalArgumentException("Hora debe estar entre 0 y 23");

        if (minuto < 0 || minuto >= 60)
            throw new IllegalArgumentException("Minuto debe estar entre 0 y 59");

        if (segundo < 0 || segundo >= 60)
            throw new IllegalArgumentException("Segundo debe estar entre 0 y 59");

        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    Tiempo(int hora, int minuto, int segundo) {
        try {
            establecerTiempo(hora, minuto, segundo);
            cantidadInstancias++;
        } catch (Exception e) {
            throw e;
        }
    }

    // this llama a otro constructor
    // constructor sobrecargado
    Tiempo(int hora, int minuto) {
        this(hora, minuto, 0);
    }

    Tiempo(int hora) {
        this(hora, 0, 0);
    }

    Tiempo() {
        this(0, 0, 0);
    }

    // Metodo estatico
    public static int getCantidadInstancias() {
        return cantidadInstancias;
    }

    // toString
    @Override
    public String toString() {
        return String.format("%d:%d:%d", this.hora, this.minuto, this.segundo);
    }

    public int getSegundo() {
        return segundo;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getHora() {
        return hora;
    }
}